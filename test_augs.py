import sys
import json
import pandas as pd
import collections
import language_tool_python


def count_repeat_ngrams(text, n = 3, repeats = 2):
    ngrams = zip(*[text.split()[i:] for i in range(n)])
    ngram_counts = collections.Counter(ngrams)
    return len([k for k, c in ngram_counts.items() if c >= repeats])>0

def flatten_list(x):
    return [item for sublist in x for item in sublist]



def check_repeat_ngrams(text):
    return not count_repeat_ngrams(text, n = 2, repeats = 3) and not count_repeat_ngrams(text, n = 3, repeats = 2)

static_language_tool = language_tool_python.LanguageTool('en-US')        
def check_grammar_errors(text, language_tool = static_language_tool):
    return len(language_tool.check(text))<=2

def check_variety(text, original_text):
    diff = len(set(original_text.split()) - set(text.split()))/len(set(original_text.split()))
    diff2 = len(set(text.split()) - set(original_text.split()))/len(set(text.split()))
    if max(diff, diff2) <0.15:
        return False
    return True

def check_literal(text):
    return 'compliance' in text.lower()


def generate_report(test_results, test_names):
    return {name: result for name, result in zip(test_names, test_results)}, all(test_results)



def main():
    if len(sys.argv[1:]) == 0:
        input_file = 'augmentations.json'
        output_file = 'augmentations_tested.json'  
    elif len(sys.argv[1:]) == 2:
        input_file = sys.argv[1]
        output_file = sys.argv[2]                
    else:
        print('Either pass 2 parameters for input and output file names, or pass not parameters for default names. Current number of parameters is %s'%len(sys.argv[1:]))
        return 1
    
    
    
    with open(input_file) as json_file:
        data = json.load(json_file)
    
    aug_df = pd.DataFrame({'original': flatten_list([[t]*len(data[t]) for t in list(data.keys())]),
             'augmentation': flatten_list([data[t] for t in list(data.keys())])
             })    
    
    aug_df['check_literal'] = aug_df.augmentation.apply(check_literal)
    aug_df['check_repeat_ngrams'] = aug_df.augmentation.apply(check_repeat_ngrams)
    aug_df['check_grammar_errors'] = aug_df.augmentation.apply(check_grammar_errors)
    aug_df['check_variety'] = aug_df.apply(lambda row: check_variety(row['augmentation'], row['original']), axis = 1)
    
    aug_df[['checks', 'result']] = aug_df.apply(lambda row: generate_report(row[2:], aug_df.columns.to_list()[2:]), axis = 1, result_type="expand")
    
    passed_df = aug_df[aug_df.result][['augmentation', 'checks']]
    failed_df = aug_df[~aug_df.result][['augmentation', 'checks']]
    
    with open(output_file, 'w') as f:
        json.dump({'passed':passed_df.to_dict(orient='records'),
            'failed':failed_df.to_dict(orient='records'),
           }, f)
        
   
if __name__ == "__main__":
    main()